import UIKit

open class StaticTableViewCell: UITableViewCell, CellProtocol {
    open func configureWith(value: Void) {
    }
}
