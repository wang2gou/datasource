import Foundation
import UIKit

open class DataSource: NSObject, UICollectionViewDataSource, UITableViewDataSource {

    private var values: [[(value: Any, reusableId: String)]] = []


    /// 注册CollectionCell
    ///
    /// - Parameters:
    ///   - cell: UICollectionViewCell 子类
    ///   - value: Cell 所需要的模型
    open func configureCell(collectionCell cell: UICollectionViewCell, withValue value: Any) {
    }


    /// 注册UITableViewCell
    ///
    /// - Parameters:
    ///   - cell: UITableViewCell 子类
    ///   - value: UITableViewCell 数据模型
    open func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
    }


    /// 自定义注册(UICollectionView)
    ///
    /// - Parameter collectionView: UICollectionView
    open func registerClasses(collectionView: UICollectionView?) {
    }


    /// 自定义注册(UITableView)
    ///
    /// - Parameter tableView: UITableView
    open func registerClasses(tableView: UITableView?) {
    }


    /// 清空所有数据模型
    public final func clearValues() {
        self.values = [[]]
    }


    /// 清空指定Section里的数据
    ///
    /// - Parameter section: Section 索引
    public final func clearValues(section: Int) {
        self.padValuesForSection(section)
        self.values[section] = []
    }

    public final func deleteValues(section: Int, inRow row: Int) {
        self.values[section].remove(at: row)
    }


    /// 在指定的Section插入数据
    ///
    /// - Parameters:
    ///   - value: 数据
    ///   - cellClass: 渲染的Cell
    ///   - section: Section
    /// - Returns: 返回对应的IndexPath
    @discardableResult
    public final func prependRow<
                                Cell: CellProtocol,
                                Value: Any>
            (value: Value, cellClass: Cell.Type, toSection section: Int) -> IndexPath
            where
            Cell.Value == Value {
        self.padValuesForSection(section)
        self.values[section].insert((value, Cell.defaultReusableId), at: 0)
        return IndexPath(row: 0, section: section)
    }


    /// 在指定的Section末尾添加数据
    ///
    /// - Parameters:
    ///   - value: 数据模型
    ///   - cellClass: Cell
    ///   - section: 区域
    /// - Returns: IndexPath
    @discardableResult
    public final func appendRow<
                               Cell: CellProtocol,
                               Value: Any>
            (value: Value, cellClass: Cell.Type, toSection section: Int) -> IndexPath
            where
            Cell.Value == Value {
        self.padValuesForSection(section)
        self.values[section].append((value, Cell.defaultReusableId))
        return IndexPath(row: self.values[section].count - 1, section: section)
    }

    @discardableResult
    public final func appendRow<
                               Value: Any>
            (value: Value, cellReusableId reusableId: String, toSection section: Int) -> IndexPath {
        self.padValuesForSection(section)
        self.values[section].append((value, reusableId))
        return IndexPath(row: self.values[section].count - 1, section: section)
    }


    @discardableResult
    public final func insertRow<Cell: CellProtocol, Value: Any>(value: Value,
                                                            cellClass: Cell.Type,
                                                            toSection section: Int,
                                                            toRow row: Int) -> IndexPath {
        self.padValuesForSection(section)
        self.values[section].insert((value, Cell.defaultReusableId), at: row)
        return IndexPath(row: row, section: section)
    }

    @discardableResult
    public final func insertRow<Value: Any>
            (value: Value, cellReusableId reusableId: String, toSection section: Int, toRow row: Int) -> IndexPath {
        self.padValuesForSection(section)
        self.values[section].insert((value, reusableId), at: row)
        return IndexPath(row: row, section: section)
    }

    @discardableResult
    public final func updateRow<Cell: CellProtocol, Value: Any>(value: Value,
                                                            cellClass: Cell.Type,
                                                            toSection section: Int,
                                                            toRow row: Int) -> IndexPath {
        self.padValuesForSection(section)
        self.values[section][row] = (value, Cell.defaultReusableId)
        return IndexPath(row: row, section: section)
    }

    /// 添加固定行
    ///
    /// - Parameters:
    ///   - cellIdentifier: Cell Identifier
    ///   - section: Section
    public final func appendStaticRow(cellIdentifier: String, toSection section: Int) {
        self.padValuesForSection(section)
        self.values[section].append(((), cellIdentifier))
    }


    /// 替换Section里面的 Identifiers
    ///
    /// - Parameters:
    ///   - cellIdentifiers: Cell Identifier
    ///   - section: Section
    public final func set(cellIdentifiers: [String], inSection section: Int) {
        self.padValuesForSection(section)
        self.values[section] = cellIdentifiers.map { ((), $0) }
    }


    /// 追加数据（新的Section）
    ///
    /// - Parameters:
    ///   - values: 数据模型
    ///   - cellClass: Cell
    public final func appendSection<
                                   Cell: CellProtocol,
                                   Value: Any>
            (values: [Value], cellClass: Cell.Type)
            where
            Cell.Value == Value {

        self.values.append(values.map { ($0, Cell.defaultReusableId) })
    }


    /// Section 模型替换
    ///
    /// - Parameters:
    ///   - values: 所有模型
    ///   - cellClass: Cell
    ///   - section: Section
    public final func set<
                         Cell: CellProtocol,
                         Value: Any>
            (values: [Value], cellClass: Cell.Type, inSection section: Int)
            where
            Cell.Value == Value {

        self.padValuesForSection(section)
        self.values[section] = values.map { ($0, Cell.defaultReusableId) }
    }


    /// Section中替换Row模型
    ///
    /// - Parameters:
    ///   - value: 数据模型
    ///   - cellClass: Cell
    ///   - section: Section
    ///   - row: Row
    public final func set<
                         Cell: CellProtocol,
                         Value: Any>
            (value: Value, cellClass: Cell.Type, inSection section: Int, row: Int)
            where
            Cell.Value == Value {

        self.values[section][row] = (value, Cell.defaultReusableId)
    }

    public final subscript(indexPath: IndexPath) -> Any {
        return self.values[indexPath.section][indexPath.item].value
    }
    public final subscript(itemSection itemSection: (item: Int, section: Int)) -> Any {
        return self.values[itemSection.section][itemSection.item].value
    }
    public final subscript(section section: Int) -> [Any] {
        return self.values[section].map { $0.value }
    }

    public final func numberOfItems() -> Int {
        return self.values.reduce(0) { accum, section in accum + section.count }
    }

    public final func itemIndexAt(_ indexPath: IndexPath) -> Int {
        return self.values[0..<indexPath.section]
                .reduce(indexPath.item) { accum, section in accum + section.count }
    }

    public final func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.values.count
    }

    public final func collectionView(_ collectionView: UICollectionView,
                                     numberOfItemsInSection section: Int) -> Int {
        return self.values[section].count
    }

    public final func collectionView(_ collectionView: UICollectionView,
                                     cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let (value, reusableId) = self.values[indexPath.section][indexPath.item]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusableId, for: indexPath)

        self.configureCell(collectionCell: cell, withValue: value)

        return cell
    }


    public final func numberOfSections(in tableView: UITableView) -> Int {
        return self.values.count
    }

    public final func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.values[section].count
    }

    public final func tableView(_ tableView: UITableView,
                                cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let (value, reusableId) = self.values[indexPath.section][indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: reusableId, for: indexPath)

        self.configureCell(tableCell: cell, withValue: value)

        return cell
    }

    internal final func reusableId(item: Int, section: Int) -> String? {
        if !self.values.isEmpty && self.values.count >= section &&
                   !self.values[section].isEmpty && self.values[section].count >= item {

            return self.values[section][item].reusableId
        }
        return nil
    }

    internal final subscript(testItemSection itemSection: (item: Int, section: Int)) -> Any? {
        let (item, section) = itemSection

        if !self.values.isEmpty && self.values.count >= section &&
                   !self.values[section].isEmpty && self.values[section].count >= item {
            return self.values[itemSection.section][itemSection.item].value
        }
        return nil
    }

    private func padValuesForSection(_ section: Int) {
        guard self.values.count <= section else { return }

        (self.values.count...section).forEach { _ in
            self.values.append([])
        }
    }
}
